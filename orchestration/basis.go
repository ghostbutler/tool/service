package orchestration

import (
	"errors"
)

type Basis struct {
	// the uuid, unique in the system
	UUID string `json:"uuid" bson:"uuid"`

	// the name, for user interaction
	Name string `json:"name" bson:"name"`

	// the description, for user interaction
	Description string `json:"description" bson:"description"`
}

func (basis *Basis) IsValid() error {
	if basis.Description == "" ||
		basis.Name == "" {
		return errors.New("empty name or description or UUID")
	} else {
		return nil
	}
}
