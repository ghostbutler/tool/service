package orchestration

import (
	"encoding/json"
	"errors"
	"time"
)

type ScheduleTime struct {
	// starting date is the date from when the schedule will
	// be considered
	// format must be DD/MM/YY
	StartingDate string `json:"startingDate" bson:"startingDate"`

	// ending date is the date schedule will be deleted at
	// format must be DD/MM/YY
	EndingDate string `json:"endingDate" bson:"endingDate"`

	// starting time is the hours/minutes/seconds of the day
	// when the schedule can be triggered
	// format must be HH:MM:SS
	StartingTime string `json:"startingTime" bson:"startingTime"`

	// ending time is the hours/minutes/seconds of the day
	// from when the schedule is disabled (IsActive set to
	// false)
	// format must be HH:MM:SS
	EndingTime string `json:"endingTime" bson:"endingTime"`

	// creation time is just an info, not actively used
	CreationDate time.Time `json:"creationDate" bson:"creationDate"`

	// is active means do we consider this schedule?
	IsActive bool `json:"isActive" bson:"isActive"`

	// periodicity is how often can the task be run
	// the unit is specified with PeriodicityUnit
	Periodicity int `json:"periodicity" bson:"periodicity"`

	// periodicity unit can be either "seconds"/"minutes"/
	// "hours"/"days"/"weeks"/"months"
	PeriodicityUnit TimeUnit `json:"periodicityUnit" bson:"periodicityUnit"`

	// days when schedule is valid
	// full day name without uppercase
	// empty means never valid!
	ValidDay []WeekDay `json:"validDay" bson:"validDay"`
}

func (scheduleTime *ScheduleTime) GetPeriodicityDuration() time.Duration {
	var unit time.Duration
	switch scheduleTime.PeriodicityUnit {
	default:
		fallthrough
	case TimeUnitSecond:
		unit = time.Second
		break
	case TimeUnitMinute:
		unit = time.Minute
		break
	case TimeUnitHour:
		unit = time.Hour
		break
	case TimeUnitDay:
		unit = time.Hour * 24
		break
	case TimeUnitWeek:
		unit = time.Hour * 24 * 7
		break
	case TimeUnitMonth:
		unit = time.Hour * 24 * 7 * 30
		break
	}
	return time.Duration(scheduleTime.Periodicity) * unit
}

func (scheduleTime *ScheduleTime) IsTodayWeekdayValid() bool {
	day := WeekDayMap[time.Now().Weekday()]
	for _, scheduleDay := range scheduleTime.ValidDay {
		if scheduleDay == day {
			return true
		}
	}
	return false
}

// date format must be
// DD:MM:AA hh:mm:ss
type Schedule struct {
	// rules identified by their ids to be valid
	// for the schedule to be triggered
	RuleId []string `json:"ruleId" bson:"ruleId"`

	// actions identified by their ids to trigger
	// if rules are valid
	ActionId []string `json:"actionId" bson:"actionId"`

	// mode is the way rules are checked
	// if mode is "permissive", only one condition
	// has to be correct for the action(s) to be launched
	// (OR)
	// if mode is "strict", all the rules have to be valid
	// for the action(s) to be launched (AND)
	RuleMode ScheduleRuleMode `json:"mode" bson:"mode"`

	// trigger reactivation mode
	// ScheduleTriggerReactivationModeTimer will allow for a redo
	// after a given time
	TriggerReactivationMode ScheduleTriggerReactivationMode `json:"triggerReactivation" bson:"triggerReactivation"`

	// schedule timing
	Time ScheduleTime `json:"time" bson:"time"`

	// are dependencies satisfied
	// a way when requesting schedule to know if all its dependencies
	// (rules/actions) are satisfied
	IsDependencySatisfied bool `json:"isDependencySatisfied" bson:"isDependencySatisfied"`

	// basis
	Basis
}

// is schedule valid?
func (schedule *Schedule) IsValid(ruleList []Rule,
	actionList []Action) error {
	if err := schedule.Basis.IsValid(); err != nil {
		return err
	}
	if schedule.ActionId == nil ||
		len(schedule.ActionId) <= 0 {
		return errors.New("empty actions list on schedule")
	} else {
		for _, action := range schedule.ActionId {
			if len(action) <= 0 {
				return errors.New("empty action id")
			}
		}
	}
	if schedule.RuleId == nil ||
		len(schedule.RuleId) <= 0 {
		return errors.New("empty rules list on schedule")
	} else {
		for _, rule := range schedule.RuleId {
			if len(rule) <= 0 {
				return errors.New("empty rule id")
			}
		}
	}
	if !schedule.TriggerReactivationMode.IsCorrect() {
		return errors.New("incorrect reactivation trigger " +
			string(schedule.TriggerReactivationMode))
	}
	if !schedule.RuleMode.IsCorrect() {
		return errors.New("incorrect schedule mode " +
			string(schedule.RuleMode))
	}
	if !schedule.Time.PeriodicityUnit.IsCorrect() {
		return errors.New("invalid periodicity unit " +
			string(schedule.Time.PeriodicityUnit))
	}
	for _, day := range schedule.Time.ValidDay {
		if !day.IsCorrect() {
			return errors.New("invalid day " +
				string(day))
		}
	}
	if ruleList != nil {
		for _, ruleID := range schedule.RuleId {
			isFound := false
			for _, rule := range ruleList {
				if rule.UUID == ruleID {
					isFound = true
				}
			}
			if !isFound {
				return errors.New("can't find rule " +
					ruleID)
			}
		}
	}
	if actionList != nil {
		for _, actionID := range schedule.ActionId {
			isFound := false
			for _, action := range actionList {
				if action.UUID == actionID {
					isFound = true
				}
			}
			if !isFound {
				return errors.New("can't find action " +
					actionID)
			}
		}
	}
	if schedule.Time.StartingDate != "" {
		if _, err := time.Parse("02/01/06",
			schedule.Time.StartingDate); err != nil {
			return errors.New("can't parse start date " +
				err.Error())
		}
	}
	if schedule.Time.EndingDate != "" {
		if _, err := time.Parse("02/01/06",
			schedule.Time.EndingDate); err != nil {
			return errors.New("can't parse end date " +
				err.Error())
		}
	}
	if schedule.Time.StartingTime != "" {
		if _, err := time.Parse("15:04:05",
			schedule.Time.StartingTime); err != nil {
			return errors.New("can't parse start time " +
				err.Error())
		}
	}
	if schedule.Time.EndingTime != "" {
		if _, err := time.Parse("15:04:05",
			schedule.Time.EndingTime); err != nil {
			return errors.New("can't parse end time " +
				err.Error())
		}
	}
	return nil
}

func ( schedule *Schedule ) MarshalJson( ) [ ]byte {
	content, _ := json.Marshal( schedule )
	return content
}