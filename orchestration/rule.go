package orchestration

import (
	"encoding/json"
	"errors"
	"gitlab.com/ghostbutler/tool/service/device"
)

type RuleFieldIdentifier string
const(
	RuleFieldIdentifierMusicSoundName = "data.musicName"
	RuleFieldIdentifierMusicEvent = "data.event"
	RuleFieldIdentifierMusicPlayerName = "data.name"
	RuleFieldIdentifierMusicVolume = "data.volume"
	RuleFieldIdentifierMusicIsWarning = "data.isWarning"
	RuleFieldIdentifierMusicUrl = "data.url"

	RuleFieldIdentifierGamepadName = "data.name"
	RuleFieldIdentifierGamepadType = "data.type"
	RuleFieldIdentifierGamepadUUID = "data.uuid"
	RuleFieldIdentifierGamepadButtonName = "data.event.name"
	RuleFieldIdentifierGamepadButtonStatus = "data.event.status"
	RuleFieldIdentifierGamepadJoystickName = "data.event.name"
	RuleFieldIdentifierGamepadJoystickX = "data.event.X"
	RuleFieldIdentifierGamepadJoystickY = "data.event.Y"

	RuleFieldIdentifierLightUUID = "data.uuid"
	RuleFieldIdentifierLightName = "data.name"
	RuleFieldIdentifierLightType = "data.type"
	RuleFieldIdentifierLightStateHue = "data.state.hue"
	RuleFieldIdentifierLightStateBrightness = "data.state.brightness"
	RuleFieldIdentifierLightStateSaturation = "data.state.saturation"
	RuleFieldIdentifierLightStateIsOn = "data.state.isOn"
	RuleFieldIdentifierLightStateIsReachable = "data.state.isReachable"

	RuleFieldIdentifierTTSName = "data.name"
	RuleFieldIdentifierTTSSentence = "data.sentence"
	RuleFieldIdentifierTTSVolume = "data.volume"
	RuleFieldIdentifierTTSLanguage = "data.language"

	RuleFieldIdentifierOutletName = "data.name"
	RuleFieldIdentifierOutletPort = "data.port"
	RuleFieldIdentifierOutletOutput = "data.output"
	RuleFieldIdentifierOutletPower = "data.power"
	RuleFieldIdentifierOutletEnergy = "data.energy"
	RuleFieldIdentifierOutletEnabled = "data.enabled"
	RuleFieldIdentifierOutletCurrent = "data.current"
	RuleFieldIdentifierOutletVoltage = "data.voltage"
	RuleFieldIdentifierOutletPowerFactor = "data.powerFactor"

	RuleFieldIdentifierTemperatureName = "data.name"
	RuleFieldIdentifierTemperatureValue = "data.temperature"

	RuleFieldIdentifierMoistureName = "data.name"
	RuleFieldIdentifierMoistureValue = "data.moisture"
)

type RuleFieldDescription struct {
	Name string
	Identifier RuleFieldIdentifier
	Description string
	Type string
}

var RuleFieldIdentifierDescription = map[device.CapabilityType] [ ]RuleFieldDescription {
	device.CapabilityTypeMusic: {
		{ Name: "Sound name", Identifier: RuleFieldIdentifierMusicSoundName, Type: "string", Description: "sound name" },
		{ Name: "Event", Identifier: RuleFieldIdentifierMusicEvent, Type: "string", Description: "load/remove/play/pause/volume/stop/finish/test" },
		{ Name: "Player name", Identifier: RuleFieldIdentifierMusicPlayerName, Type: "string", Description: "the player name" },
		{ Name: "Music volume", Identifier: RuleFieldIdentifierMusicVolume, Type: "int", Description: "the music volume between 1 and 100" },
		{ Name: "Is warning?", Identifier: RuleFieldIdentifierMusicIsWarning, Type: "bool", Description: "did a warning happen?" },
		{ Name: "Music url", Identifier: RuleFieldIdentifierMusicUrl, Type: "string", Description: "the music url" },
	},

	device.CapabilityTypeTextToSpeech: {
		{ Name: "TTS name", Identifier: RuleFieldIdentifierTTSName, Type: "string", Description: "the text to speech device name" },
		{ Name: "Sentence", Identifier: RuleFieldIdentifierTTSSentence, Type: "string", Description: "the spoken sentence" },
		{ Name: "Volume", Identifier: RuleFieldIdentifierTTSVolume, Type: "int", Description: "the volume" },
		{ Name: "Language", Identifier: RuleFieldIdentifierTTSLanguage, Type: "string", Description: "the spoken sentence language" },
	},

	device.CapabilityTypeOutlet: {
		{ Name: "Name", Identifier: RuleFieldIdentifierOutletName, Type: "string", Description: "the outlet name" },
		{ Name: "Port", Identifier: RuleFieldIdentifierOutletPort, Type: "int", Description: "the outlet port (as wrote on outlet)" },
		{ Name: "Output status", Identifier: RuleFieldIdentifierOutletOutput, Type: "int", Description: "the outlet output status (0 or 1)" },
		{ Name: "Power", Identifier: RuleFieldIdentifierOutletPower, Type: "float", Description: "the current delivered power" },
		{ Name: "Energy", Identifier: RuleFieldIdentifierOutletEnergy, Type: "float", Description: "the current delivered energy" },
		{ Name: "Is enabled?", Identifier: RuleFieldIdentifierOutletEnabled, Type: "int", Description: "is the outlet enabled ? (0 or 1)" },
		{ Name: "Current", Identifier: RuleFieldIdentifierOutletCurrent, Type: "float", Description: "the current" },
		{ Name: "Voltage", Identifier: RuleFieldIdentifierOutletVoltage, Type: "float", Description: "the outlet voltage" },
		{ Name: "Power factor", Identifier: RuleFieldIdentifierOutletPowerFactor, Type: "float", Description: "the power factor" },
	},
	device.CapabilityTypeLight: {
		{ Name: "UUID", Identifier: RuleFieldIdentifierLightUUID, Type: "string", Description: "the light uuid" },
		{ Name: "Name", Identifier: RuleFieldIdentifierLightName, Type: "string", Description: "the light name" },
		{ Name: "Type", Identifier: RuleFieldIdentifierLightType, Type: "string", Description: "the light type (bulb/globe/stripe/candle)" },
		{ Name: "Hue", Identifier: RuleFieldIdentifierLightStateHue, Type: "int", Description: "the hue (between 0 and 360)" },
		{ Name: "Brightness", Identifier: RuleFieldIdentifierLightStateBrightness, Type: "int", Description: "the brightness (between 0 and 255)" },
		{ Name: "Saturation", Identifier: RuleFieldIdentifierLightStateSaturation, Type: "int", Description: "the saturation (between 0 and 255)" },
		{ Name: "Is light on?", Identifier: RuleFieldIdentifierLightStateIsOn, Type: "bool", Description: "if light is enabled" },
		{ Name: "Is light reachable?", Identifier: RuleFieldIdentifierLightStateIsReachable, Type: "bool", Description: "if the light is reachable" },
	},

	device.CapabilityTypeGamepad: {
		{ Name: "Gamepad name", Identifier: RuleFieldIdentifierGamepadName, Type: "string", Description: "the element name" },
		{ Name: "Event type", Identifier: RuleFieldIdentifierGamepadType, Type: "string", Description: "the gamepad event type (button or joystick)" },
		{ Name: "UUID", Identifier: RuleFieldIdentifierGamepadUUID, Type: "string", Description: "the gamepad uuid" },
		{ Name: "Button name", Identifier: RuleFieldIdentifierGamepadButtonName, Type: "string", Description: "the gamepad button name" },
		{ Name: "Button event", Identifier: RuleFieldIdentifierGamepadButtonStatus, Type: "string", Description: "the gamepad button status (pressed/lpressed/released/lreleased/dpressed)" },
		{ Name: "Joystick name", Identifier: RuleFieldIdentifierGamepadJoystickName, Type: "string", Description: "the gamepad joystick name" },
		{ Name: "Joystick X", Identifier: RuleFieldIdentifierGamepadJoystickX, Type: "float", Description: "the gamepad joystick X value" },
		{ Name: "Joystick Y", Identifier: RuleFieldIdentifierGamepadJoystickY, Type: "float", Description: "the gamepad joystick Y value" },
	},

	device.CapabilityTypeTemperature: {
		{ Name: "Temperature sensor name", Identifier: RuleFieldIdentifierTemperatureName, Type: "string", Description: "the element name" },
		{ Name: "Temperature sensor value", Identifier: RuleFieldIdentifierTemperatureValue, Type: "string", Description: "the element value" },
	},

	device.CapabilityTypeMoisture: {
		{ Name: "Moisture sensor name", Identifier: RuleFieldIdentifierMoistureName, Type: "string", Description: "the element name" },
		{ Name: "Moisture sensor value", Identifier: RuleFieldIdentifierMoistureValue, Type: "string", Description: "the element value" },
	},
}

type RulePattern struct {
	// sensor name
	Name string `json:"name" bson:"name"`

	// value
	Value interface{} `json:"value" bson:"value"`
}

type RuleCondition struct {
	// check mode is the way data will be checked
	// if check mode is "variation", it will look for a
	// change in status in a given interval
	// if check mode is "comparison", it will check if a value
	// is less than/equals/more than a given threshold
	CheckMode CheckMode `json:"checkMode" bson:"checkMode"`

	// comparison is the comparison mode for "comparison"
	// check mode
	// can be "mt" for more than, "lt" for less than,
	// or "equals"
	Comparison ComparisonMode `json:"comparison" bson:"comparison"`

	// field is the data to be check when testing data
	// if this is a complex type, it will be the condition
	// interpreter responsibility to ensure it can access
	// right data with simple field name
	// for example "button"/"outlet"/...
	Field string `json:"field" bson:"field"`

	// value is
	// for "variation" check mode: the initial value to
	// consider when looking for a variation
	// for "comparison" check mode: the threshold to test
	// data with
	Value interface{} `json:"value" bson:"value"`

	// time frame for "for" mode comparison (unit is second)
	ForLessThan int64 `json:"forLessThan" bson:"forLessThan"`
	ForMoreThan int64 `json:"forMoreThan" bson:"forMoreThan"`

	// sensor names/values pattern for Pattern mode
	Pattern []RulePattern `json:"pattern" bson:"pattern"`

	// is the pattern strict (values must follow one another exactly
	// as specified) or permissive where the goal is only to find the
	// pattern no matter if there are other values between
	IsStrictPattern bool `json:"isStrictPattern" bson:"isStrictPattern"`
}

type RuleContent struct {
	// name is the element(s) name(s) of the given type
	// if you want to consider multiple elements to put
	// same conditions on, the array is here for
	Name []string `json:"name" bson:"name"`

	// the check period is the time period in seconds for which
	// the check will be run in database
	// it will look for condition validity in the given period
	// of time only
	// for example, if you put here 5, the orchestrator will
	// ask mongo for a check on last 5 seconds over all events
	// of different contents declared
	CheckPeriod int64 `json:"checkPeriod" bson:"checkPeriod"`

	// the type is the data type rule will apply on
	// for example, you could find here "outlet"
	// or "light"
	Type string `json:"type" bson:"type"`

	// these are the condition(s) to test on the given
	// element type
	// look at condition struct for more details
	Condition []RuleCondition `json:"condition" bson:"condition"`
}

type Rule struct {
	// content is the AND list of conditions to validate
	// for the rule to be valid
	Content []RuleContent `json:"rule" bson:"rule"`

	// basis
	Basis
}

// check if rule is valid
func (rule *Rule) IsValid() error {
	// check basis
	if err := rule.Basis.IsValid(); err != nil {
		return err
	}

	// check content
	if rule.Content == nil {
		return errors.New("no rule content")
	} else {
		// iterate rules
		for _, content := range rule.Content {
			if device.IsCapacityValid(content.Type) {
				if content.Name == nil ||
					len(content.Name) <= 0 {
					return errors.New("bad condition sensor/actuator name")
				} else {
					for _, name := range content.Name {
						if name == "" {
							return errors.New("bad condition sensor/actuator name length")
						}
					}
				}
				// iterate conditions
				for _, condition := range content.Condition {
					if condition.Value == "" ||
						condition.Field == "" {
						return errors.New("bad condition value or field")
					}
					if !condition.CheckMode.IsCorrect() {
						return errors.New("unknown check mode" +
							string(condition.CheckMode))
					}
					switch condition.CheckMode {
					case CheckModeLastValue:
					case CheckModeForComparison:
						if content.CheckPeriod > 0 {
							return errors.New("last/for mode will consider last value, no need check period")
						}
						break
					case CheckModePattern:
						if content.CheckPeriod < 1 {
							return errors.New("pattern mode need a time frame to look for pattern into")
						}
						break
					}
				}
			} else {
				return errors.New("bad rule sensor type")
			}
		}
	}

	// no error
	return nil
}

func (rule *Rule) MarshalJson( ) [ ]byte {
	content, _ := json.Marshal( rule )
	return content
}
