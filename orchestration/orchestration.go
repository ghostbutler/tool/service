package orchestration

import (
	"time"
)

type CheckMode string

const (
	// the value has been in a certain state for more
	// or less than a given time
	CheckModeForComparison = CheckMode("for")

	// will check the last value only (no time frame)
	CheckModeLastValue = CheckMode("last")

	// check if values followed a given pattern in the given time
	// frame. for example on a gamepad, this could be a secret code
	// to input like up/right/down/left
	CheckModePattern = CheckMode("pattern")
)

func (checkMode *CheckMode) IsCorrect() bool {
	switch *checkMode {
	case CheckModeLastValue:
		fallthrough
	case CheckModeForComparison:
		fallthrough
	case CheckModePattern:
		return true

	default:
		return false
	}
}

type FieldType string

const (
	FieldTypeInt    = FieldType("int")
	FieldTypeFloat  = FieldType("float")
	FieldTypeBool   = FieldType("bool")
	FieldTypeString = FieldType("string")
)

func (fieldType *FieldType) IsCorrect() bool {
	switch *fieldType {
	case FieldTypeInt:
		fallthrough
	case FieldTypeFloat:
		fallthrough
	case FieldTypeBool:
		fallthrough
	case FieldTypeString:
		return true

	default:
		return false
	}
}

type TimeUnit string

const (
	TimeUnitSecond = TimeUnit("seconds")
	TimeUnitMinute = TimeUnit("minutes")
	TimeUnitHour   = TimeUnit("hours")
	TimeUnitDay    = TimeUnit("days")
	TimeUnitWeek   = TimeUnit("weeks")
	TimeUnitMonth  = TimeUnit("months")
)

func (timeUnit *TimeUnit) IsCorrect() bool {
	switch *timeUnit {
	case TimeUnitSecond:
		fallthrough
	case TimeUnitMinute:
		fallthrough
	case TimeUnitHour:
		fallthrough
	case TimeUnitDay:
		fallthrough
	case TimeUnitWeek:
		fallthrough
	case TimeUnitMonth:
		return true

	default:
		return false
	}
}

type ScheduleRuleMode string

const (
	ScheduleRuleModeStrict     = ScheduleRuleMode("strict")
	ScheduleRuleModePermissive = ScheduleRuleMode("permissive")
)

// is schedule mode correct?
func (scheduleMode ScheduleRuleMode) IsCorrect() bool {
	switch scheduleMode {
	case ScheduleRuleModePermissive:
		fallthrough
	case ScheduleRuleModeStrict:
		return true

	default:
		return false
	}
}

type ScheduleTriggerReactivationMode string

const (
	ScheduleTriggerReactivationModeTimer          = ScheduleTriggerReactivationMode("timer")
	ScheduleTriggerReactivationModeInvalidateRule = ScheduleTriggerReactivationMode("invalidate")
)

func (scheduleTriggerReactivationMode *ScheduleTriggerReactivationMode) IsCorrect() bool {
	switch *scheduleTriggerReactivationMode {
	case ScheduleTriggerReactivationModeTimer:
		fallthrough
	case ScheduleTriggerReactivationModeInvalidateRule:
		return true

	default:
		return false
	}
}

type ComparisonMode string

const (
	ComparisonModeEqual       = ComparisonMode("eq")
	ComparisonModeGreaterThan = ComparisonMode("gt")
	ComparisonModeLessThan    = ComparisonMode("lt")
)

func (comparisonMode *ComparisonMode) IsCorrect() bool {
	switch *comparisonMode {
	case ComparisonModeEqual:
		fallthrough
	case ComparisonModeGreaterThan:
		fallthrough
	case ComparisonModeLessThan:
		return true

	default:
		return false
	}
}

type WeekDay string

const (
	WeekDayMonday    = WeekDay("monday")
	WeekDayTuesday   = WeekDay("tuesday")
	WeekDayWednesday = WeekDay("wednesday")
	WeekDayThursday  = WeekDay("thursday")
	WeekDayFriday    = WeekDay("friday")
	WeekDaySaturday  = WeekDay("saturday")
	WeekDaySunday    = WeekDay("sunday")
)

var WeekDayMap = map[time.Weekday]WeekDay{
	time.Sunday:    WeekDaySunday,
	time.Monday:    WeekDayMonday,
	time.Tuesday:   WeekDayTuesday,
	time.Wednesday: WeekDayWednesday,
	time.Thursday:  WeekDayThursday,
	time.Friday:    WeekDayFriday,
	time.Saturday:  WeekDaySaturday,
}

// is week day correct?
func (weekDay *WeekDay) IsCorrect() bool {
	switch *weekDay {
	case WeekDayMonday:
		fallthrough
	case WeekDayTuesday:
		fallthrough
	case WeekDayWednesday:
		fallthrough
	case WeekDayThursday:
		fallthrough
	case WeekDayFriday:
		fallthrough
	case WeekDaySaturday:
		fallthrough
	case WeekDaySunday:
		return true

	default:
		return false
	}
}
