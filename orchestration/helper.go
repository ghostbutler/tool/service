package orchestration

import "math/rand"

const (
	authorizedUUIDCharacter = "azertyuiopqsdfghjklmwxcvbn1234567890AZERTYUIOPQSDFGHJKLMWXCVBN"
	uuidLength              = 32
)

func GenerateRandomUUID() string {
	out := ""
	for i := 0; i < uuidLength; i++ {
		out += string(authorizedUUIDCharacter[rand.Int()%len(authorizedUUIDCharacter)])
	}
	return out
}
