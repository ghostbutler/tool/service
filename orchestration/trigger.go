package orchestration

import (
	"time"
)

type Trigger struct {
	UUID         string    `json:"uuid" bson:"uuid"`
	ScheduleID   string    `json:"scheduleID" bson:"scheduleID"`
	CreationTime time.Time `json:"creationTime" bson:"creationTime"`
	UnlockTime   time.Time `json:"unlockTime" bson:"unlockTime"`
	IsLock       bool      `json:"isLock" bson:"isLock"`
}
