package orchestration

import (
	"encoding/json"
	"errors"
	"gitlab.com/ghostbutler/tool/service/device"
)

type ActionIdentifier string

const (
	// test identifier (common)
	ActionIdentifierTest = "test"
	
	/* light */
	// no parameter
	ActionIdentifierLightOn = "on"
	// no parameter
	ActionIdentifierLightOff = "off"
	// color=[r=&g=&b=/hue=&saturation=]
	ActionIdentifierLightColor = "color"
	ActionIdentifierLightBlink = "blink"
	// brightness=int
	ActionIdentifierLightBrightness = "brightness"
	// name=string
	ActionIdentifierLightChangeName = "name"

	/* outlet */
	// no parameter
	ActionIdentifierOutletOn = "on"
	// no parameter
	ActionIdentifierOutletOff = "off"

	// music actions identifiers
	// name=string,url=string,isMustPlay=bool,volume=int
	ActionIdentifierMusicLoad = "load"
	// name=string,volume=int
	ActionIdentifierMusicPlay = "play"
	// name=string
	ActionIdentifierMusicPause = "pause"
	// name=string
	ActionIdentifierMusicStop = "stop"
	// name=string
	ActionIdentifierMusicDelete = "delete"
	// name=string
	ActionIdentifierMusicSetPlayerName = "name"
	// name=string,volume=int
	ActionIdentifierMusicVolume = "volume"

	/* text to speech */
	// name=string
	ActionIdentifierTTSChangeName = "name"
	// sentence=string,language=string,volume=int
	ActionIdentifierTTSTalk = "tts"

	/* game pad */
	// name=string
	ActionIdentifierGamePadChangeName = "name"

	/* temperature */
	// name=string
	ActionIdentifierTemperatureChangeName = "name"

	/* moisture */
	// name=string
	ActionIdentifierMoistureChangeName = "name"
)

var ActionIdentifierMap = map[device.CapabilityType]map[string]ActionIdentifier{
	// light
	device.CapabilityTypeLight: {
		"test":       ActionIdentifierTest,
		"on":         ActionIdentifierLightOn,
		"off":        ActionIdentifierLightOff,
		"color":      ActionIdentifierLightColor,
		"blink":      ActionIdentifierLightBlink,
		"brightness": ActionIdentifierLightBrightness,
		"name":       ActionIdentifierLightChangeName,
	},

	// outlet
	device.CapabilityTypeOutlet: {
		"test":       ActionIdentifierTest,
		"on":  ActionIdentifierOutletOn,
		"off": ActionIdentifierOutletOff,
	},

	// music
	device.CapabilityTypeMusic: {
		"test":       ActionIdentifierTest,
		"load":   ActionIdentifierMusicLoad,
		"play":   ActionIdentifierMusicPlay,
		"pause":  ActionIdentifierMusicPause,
		"stop":   ActionIdentifierMusicStop,
		"delete": ActionIdentifierMusicDelete,
		"name":   ActionIdentifierMusicSetPlayerName,
		"volume": ActionIdentifierMusicVolume,
	},

	// text to speech
	device.CapabilityTypeTextToSpeech: {
		"test":       ActionIdentifierTest,
		"name": ActionIdentifierTTSChangeName,
		"tts":  ActionIdentifierTTSTalk,
	},

	// game pad
	device.CapabilityTypeGamepad: {
		"name": ActionIdentifierGamePadChangeName,
	},

	// temperature
	device.CapabilityTypeTemperature: {
		"name": ActionIdentifierTemperatureChangeName,
	},

	// moisture
	device.CapabilityTypeMoisture: {
		"name": ActionIdentifierMoistureChangeName,
	},
}

type ActionIdentifierParameterDescription struct {
	Name string
	Type string
	Description string
}

type ActionIdentifierDescription struct {
	Name string
	Description string
	ParameterList [ ]ActionIdentifierParameterDescription
}

var ActionDescription = map[ device.CapabilityType ] [ ]ActionIdentifierDescription {
	device.CapabilityTypeLight: {
		ActionIdentifierDescription{ Name: ActionIdentifierLightBlink, Description: "blink the light", ParameterList: [ ]ActionIdentifierParameterDescription{ } },
		ActionIdentifierDescription{ Name: ActionIdentifierLightOn, Description: "light on", ParameterList: [ ]ActionIdentifierParameterDescription{ } },
		ActionIdentifierDescription{ Name: ActionIdentifierLightOff, Description: "light off", ParameterList: [ ]ActionIdentifierParameterDescription{ } },
		ActionIdentifierDescription{ Name: ActionIdentifierLightColor, Description: "change color", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "color", Description: "the new color (\"hue=int[0-360]&saturation=int[0-255]\" OR \"r=int[0-255]&g=int[0-255]&b=int[0-255]\"", Type: "string" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierLightBrightness, Description: "change brightness", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "brightness", Type: "int", Description: "the brightness [1-255]"} } },
		ActionIdentifierDescription{ Name: ActionIdentifierLightChangeName, Description: "change name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the new name" } } },
	},

	// outlet
	device.CapabilityTypeOutlet: {
		ActionIdentifierDescription{ Name: ActionIdentifierOutletOn, Description: "outlet on", ParameterList: [ ]ActionIdentifierParameterDescription{ } },
		ActionIdentifierDescription{ Name: ActionIdentifierOutletOff, Description: "outlet off", ParameterList: [ ]ActionIdentifierParameterDescription{ } },
	},

	// music
	device.CapabilityTypeMusic: {
		ActionIdentifierDescription{ Name: ActionIdentifierMusicLoad, Description: "load a sound from external source", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the name of the sound after download (must end with .mp3)" }, { Name: "url", Type: "string", Description: "the http url" }, { Name: "isMustPlay", Type: "bool", Description: "is the sound run after download?" }, { Name: "volume", Type: "int", Description: "the volume [1-100]" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierMusicPlay, Description: "play sound by its name", ParameterList: [ ]ActionIdentifierParameterDescription{  { Name: "name", Type: "string", Description: "the name of the sound" }, { Name: "volume", Type: "int", Description: "the volume [1-100]" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierMusicPause, Description: "pause a sound by its name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the name of the sound" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierMusicStop, Description: "stop a sound by its name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the name of the sound" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierMusicDelete, Description: "delete a sound by its name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the name of the sound" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierMusicSetPlayerName, Description: "set player name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the new player name" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierMusicVolume, Description: "set sound volume", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the name of the sound" }, { Name: "volume", Type: "string", Description: "the new volume for the sound" } } },
	},

	// text to speech
	device.CapabilityTypeTextToSpeech: {
		ActionIdentifierDescription{ Name: ActionIdentifierTTSChangeName, Description: "change tts controller name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the new controller name" } } },
		ActionIdentifierDescription{ Name: ActionIdentifierTTSTalk, Description: "change tts controller name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "sentence", Type: "string", Description: "the sentence" }, { Name: "language", Type: "string", Description: "the language" }, { Name: "volume", Type: "int", Description: "the volume [1-100]" } } },
	},

	// game pad
	device.CapabilityTypeGamepad: {
		ActionIdentifierDescription{ Name: ActionIdentifierGamePadChangeName, Description: "change gamepad controller name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the new controller name" } } },
	},

	// temperature
	device.CapabilityTypeTemperature: {
		ActionIdentifierDescription{ Name: ActionIdentifierTemperatureChangeName, Description: "change temperature sensor name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the new sensor name" } } },
	},

	// moisture
	device.CapabilityTypeMoisture: {
		ActionIdentifierDescription{ Name: ActionIdentifierMoistureChangeName, Description: "change moisture sensor name", ParameterList: [ ]ActionIdentifierParameterDescription{ { Name: "name", Type: "string", Description: "the new sensor name" } } },
	},
}

type ActionContentAtom struct {
	// the name of the actuator of the given type to
	// act on
	// no name means all actuators of given type
	Name string `json:"name" bson:"name"`

	// action identifier
	Identifier ActionIdentifier `json:"identifier" bson:"identifier"`

	// field
	FieldList map[string]interface{} `json:"field" bson:"field"`

	// delay is the latency before action is executed
	Delay int `json:"delay" bson:"delay"`

	// delay unit can be one of "seconds"/"minutes"/"hours"/"days"/"weeks"/"months"
	DelayUnit TimeUnit `json:"delayUnit" bson:"delayUnit"`
}

type ActionContent struct {
	// this is the actuator type, for example "outlet", "light"
	Type string `json:"type" bson:"type"`

	// atomic actions list
	Action []ActionContentAtom `json:"action" bson:"action"`
}

type Action struct {
	// action content
	Content []ActionContent `json:"action" bson:"action"`

	Basis
}

// check if action is valid
func (action *Action) IsValid() error {
	// check basis
	if err := action.Basis.IsValid(); err != nil {
		return err
	}

	// check content
	if action.Content == nil {
		return errors.New("no action content")
	} else {
		if len(action.Content) <= 0 {
			return errors.New("empty action")
		}

		// iterate content
		for _, content := range action.Content {
			if device.IsCapacityValid(content.Type) {
				// check for actions
				if len(content.Action) <= 0 {
					return errors.New("no action for type " +
						content.Type)
				}
				// iterate actions
				for _, action := range content.Action {
					/*if len( action.Field ) <= 0 {
						return errors.New( "no action field" )
					} else {
						if len( action.Field ) <= 0 {
							return errors.New( "empty action field" )
						}
					}*/
					if action.Delay != 0 &&
						!action.DelayUnit.IsCorrect() {
						return errors.New("incorrect action delay unit")
					}
				}
			} else {
				return errors.New("capacity " +
					content.Type +
					" is not valid")
			}
		}
	}

	// no error
	return nil
}

func ( action *Action ) MarshalJson( ) [ ]byte {
	content, _ := json.Marshal( action )
	return content
}