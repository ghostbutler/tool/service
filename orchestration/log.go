package orchestration

type ActionLog struct {
	// action uuid
	UUID string `json:"uuid" bson:"uuid"`

	// activation date
	LastActivationDate string `json:"activationDate" bson:"activationDate"`

	// activation count
	ActivationCount int `json:"activationCount" bson:"activationCount"`
}

// todo
type ScheduleLog struct {
}
