oOrchestration
=============

Introduction
------------

You can find here all the data related to actions/rules orchestration

Example
-------

**Action:**

```json
{
  "name": "Test",
  "description": "MDR",
  "uuid": "1234",
  "action": [
    {
      "type": "light",
      "action": [
        {
          "identifier": "off",
          "name": "",
          "field": {
            "color": "r=255&g=255&b=255"
          }
        }
      ]
    }
  ]
}
```

Via directory /api/v1/directory/action endpoint

```bash
curl -k -H "X-Ghost-Token: `accessgenerator 127.0.0.1 GhostButler2018Scanner "GhostButlerProject2018*"`" https://127.0.0.1:16564/api/v1/directory/action -X PUT -d "{\"name\":\"Nuage\", \"type\":\"light\", \"identifier\":\"color\", \"field\": { \"color\": \"r=220&g=255&b=220\" } }"
```
