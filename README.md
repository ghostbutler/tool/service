Ghost service common
====================

Introduction
------------

This is the common data for all ghost services

It also provides the ability to create easily a service.

Import
------

To import this library, just type

```bash
go get gitlab.com/ghostbutler/tool/service
```

Service
-------

In this library, you'll find all ghost butler services definitions, with its name, default listening port, and default
hostname to look for service at.

This part is written into service.go, and to add a service, just follow the template already present in it.

Endpoint
--------

This package provides basic API endpoint creation with URL->API service linkage.

By default, it adds default comportment for /api/v1/status, which gives the current status of the service, and which
does not have to be modified.

To add a service into your code, just use this template:

```go
// put your package name here
package service

// we do use this library to declare our endpoints
import (
	"gitlab.com/ghostbutler/tool/service"
)

// set here your service version
const(
	VersionMajor = 1
	VersionMinor = 0
)

// define your services ids, must start from last built in index
const (
	APIServiceV1YourFirstEndpoint = common.APIServiceType( iota + common.APIServiceBuiltInLast )
	APIServiceV1YourSecondEndpoint
	APIServiceV1YourNthEndpoint
)

// declare how you want your endpoints to be accessed to
// Path is the split version of URL.Path by '/' character. So to express /api/v1/first, you'll put here 
// ["api", "v1", "first"]
// Method is the HTTP method to use to access this endpoint
// Callback is a function of type common.EndpointFunction which will be called when an HTTP request triggers an endpoint
var SecurityManagerAPIService = map[ common.APIServiceType ] *common.APIEndpoint {
	APIServiceV1SecurityManager: {
		Path: [ ]string{ "api", "v1", "first" },
		Method: "GET",
		Description: "What your endpoint does",
		Callback: YourFirstCallback,
	},
	APIServiceV1SecurityManagerGetAPIKey: {
		Path: [ ]string{ "api", "v1", "second" },
		Method: "POST",
		Description: "What your endpoint does",
		Callback: YourSecondCallback,
	},
}

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	service *common.Service
}

// this is the function you will use to build your service
func BuildService( listeningPort int ) *Service {
	// allocate service
	service := &Service{

	}

	// build service with previous specified data
	service.service = common.BuildService( listeningPort,
		common.ServiceSecurityManager,
		SecurityManagerAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		service )

	// service is built
	return service
}
```

This code will activate your entry points, but will also start the HTTPS server on the port you specified.

Default endpoints
-----------------

By default you can find two endpoints:

- GET /api/v1

  Get a list of all endpoints provided by the service.

  Example:

```bash
  curl https://127.0.0.1:ServicePort/api/v1 \
      --insecure
```

  Response example:

```json
{
	"endpoints": [
		{
			"method": "POST",
			"endpoint": "/api/v1/securitymanager/api",
			"description": "Create a public/private api key set based on basic authentication result"
		},
		{
			"method": "POST",
			"endpoint": "/api/v1/securitymanager/api/once",
			"description": "Create a public api key valid for only one used based on public/private key delivered previously"
		},
		{
			"method": "GET",
			"endpoint": "/api/v1/securitymanager/api",
			"description": "Get public key details if valid. If type is OneTime use, key is removed after first call"
		},
		{
			"method": "GET",
			"endpoint": "/api/v1",
			"description": "Give a complete list of all the endpoints existing"
		},
		{
			"method": "GET",
			"endpoint": "/api/v1/status",
			"description": "Get the current status of the service"
		}
	]
}
```

  |Name|Type|Description|
  |----|----|-----------|
  |endpoints.method|string|The http method to contact this endpoint|
  |endpoints.endpoint|string|The path to contact this endpoint|
  |endpoints.description|string|A text description of the endpoint|

- GET /api/v1/status

  Get current status of the service

  Example:

```bash
curl https://127.0.0.1:16560/api/v1/status \
    --insecure
```

  Response example:

```json
{
  "version": "1.0",
  "type": "ghostbutler",
  "name": "ghostbutler.securitymanager",
  "status": "ok",
  "id": "MACHINE_ID"
}
```

  |Name|Type|Description|
  |----|----|-----------|
  |version|string|The service version (major.minor)|
  |type|string|The service type (for us, it will always be ghostbutler)|
  |name|string|The service name|
  |status|string|Current status, by default this gives `ok`|
  |id|string|A unique ID which uniquely identify the service (which is the machine id, one service can only have one instance per-machine)|

  Headers:

  - X-Service-Id: MACHINE_ID

  The X-Ghost-Butler header is only there to simplify detection by other services, it specifies the component ID.

Author
------

Ghost Butler <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/tool/service/common.git
