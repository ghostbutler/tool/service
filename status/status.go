package status

import (
	"encoding/json"
	"github.com/denisbrodbeck/machineid"
	"strconv"
)

type Status struct {
	// major.minor
	Version string `json:"version"`

	// device type
	ServiceType string `json:"type"`

	// service name
	ServiceName string `json:"name"`

	// current status (default: "ok")
	Status string `json:"status"`

	// service ID
	ID string `json:"id"`
}

// build status
func Build(versionMajor int,
	versionMinor int,
	serviceType string,
	serviceName string) *Status {
	id, _ := machineid.ProtectedID(serviceName)
	return &Status{
		Version: strconv.Itoa(versionMajor) +
			"." +
			strconv.Itoa(versionMinor),
		ServiceType: serviceType,
		ServiceName: serviceName,
		Status:      "ok",
		ID:          id,
	}
}

// generate json
func (s *Status) GenerateJson() []byte {
	out, _ := json.Marshal(s)
	return out
}
