Status service
==============

Introduction
------------

This is the status service, the response to be given on a
`/status` request, common to all ghost services.

Import
------

To import this library, use

```bash
go get gitlab.com/ghostbutler/tool/service/status
```

Status example
--------------

```json
{
	"version": "1.0",
	"type":"ghostbutler",
	"name":"ghostbutler.securitymanager",
	"status":"ok",
	"id":5577006791947779410
}
```

|Name|Type|Description|
|----|----|-----------|
|version|string|The major.minor version|
|type|string|The type of the component. For ghost butler it will always be "ghostbutler"|
|name|string|The full component name, useful for scanner detection|
|status|string|The current status of the component. By default, set to `ok`|
|id|uint64|A unique ID which identify the service (randomly generated at startup|

Author
------

Ghost butler <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/tool/service/status.git

