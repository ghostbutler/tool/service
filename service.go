package common

import (
	"errors"
)

const (
	GhostServiceTypeName = "ghostbutler"
)

type ServiceType int

const (
	ServiceScanner = ServiceType(iota)
	ServiceSecurityManager
	ServiceControllerMFI
	ServiceControllerHUE
	ServiceDirectory
	ServiceOrchestrator
	ServiceEvent
	ServiceSensor
	ServiceControllerGhostMusic
	ServiceControllerGhostGamepad
	ServiceObjectTextToSpeech
	ServiceFrontEndWebAPI
	ServiceControllerFlic
	ServiceObjectGarden
	ServiceIconProvider
	ServiceDeviceStatus
)

type ServiceDescription struct {
	// default port
	DefaultPort int

	// service name
	Name string

	// default hostname
	DefaultHostName string
}

var GhostService = map[ServiceType]*ServiceDescription{
	// the scanner is responsible for services detection based
	// on given rules
	ServiceScanner: {
		DefaultPort:     16561,
		Name:            "ghostbutler.scanner",
		DefaultHostName: "scanner.ghostbutler",
	},
	// the security manager is responsible for inter-services
	// communication, and provides one time key valid for
	// short period of time
	ServiceSecurityManager: {
		DefaultPort:     16560,
		Name:            "ghostbutler.securitymanager",
		DefaultHostName: "securitymanager.ghostbutler",
	},
	// mfi controller controls the mfi outlets
	ServiceControllerMFI: {
		DefaultPort:     16562,
		Name:            "ghostbutler.mficontroller",
		DefaultHostName: "mficontroller.ghostbutler",
	},
	// hue controller controls philips hue bulb/strip/globe/...
	ServiceControllerHUE: {
		DefaultPort:     16563,
		Name:            "ghostbutler.huecontroller",
		DefaultHostName: "huecontroller.ghostbutler",
	},
	// the directory service is responsible for devices
	// centralization
	ServiceDirectory: {
		DefaultPort:     16564,
		Name:            "ghostbutler.directory",
		DefaultHostName: "directory.ghostbutler",
	},
	// the orchestrator takes rules/actions/schedule
	// and starts actions if rules are valid
	ServiceOrchestrator: {
		DefaultPort:     16565,
		Name:            "ghostbutler.orchestrator",
		DefaultHostName: "orchestrator.ghostbutler",
	},
	// event handler consume incoming message into event queue
	// and run them after given period of time
	ServiceEvent: {
		DefaultPort:     16566,
		Name:            "ghostbutler.event",
		DefaultHostName: "event.ghostbutler",
	},
	// sensor handler consume incoming message into sensor queue
	// and persist them into mongo base
	ServiceSensor: {
		DefaultPort:     16567,
		Name:            "ghostbutler.sensor",
		DefaultHostName: "sensor.ghostbutler",
	},

	ServiceControllerGhostMusic: {
		DefaultPort:     16568,
		Name:            "ghostbutler.ghostmusic",
		DefaultHostName: "ghostmusic.ghostbutler",
	},
	ServiceControllerGhostGamepad: {
		DefaultPort:     16569,
		Name:            "ghostbutler.ghostgamepad",
		DefaultHostName: "ghostgamepad.ghostbutler",
	},

	ServiceObjectTextToSpeech: {
		DefaultPort:     16570,
		Name:            "ghostbutler.speechtotext",
		DefaultHostName: "speechtotext.ghostbutler",
	},

	ServiceFrontEndWebAPI: {
		DefaultPort:     16580,
		Name:            "ghostbutler.httpapi",
		DefaultHostName: "httpapi.ghostbutler",
	},

	ServiceControllerFlic: {
		DefaultPort:     16571,
		Name:            "ghostbutler.fliccontroller",
		DefaultHostName: "fliccontroller.ghostbutler",
	},

	ServiceObjectGarden: {
		DefaultPort:     16572,
		Name:            "ghostbutler.garden",
		DefaultHostName: "garden.ghostbutler",
	},

	ServiceIconProvider: {
		DefaultPort:     16581,
		Name:            "ghostbutler.iconprovider",
		DefaultHostName: "iconprovider.ghostbutler",
	},

	ServiceDeviceStatus: {
		DefaultPort:     16573,
		Name:            "ghostbutler.status",
		DefaultHostName: "status.ghostbutler",
	},
}

// find service type from service name
func FindServiceTypeFromName(serviceName string) (ServiceType, error) {
	for serviceType, service := range GhostService {
		if service.Name == serviceName {
			return serviceType, nil
		}
	}
	return ServiceType(0), errors.New("bad name")
}
