package keystore

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"time"
)

const (
	// security manager constants
	securityManagerPort          = 16560
	securityManagerBasicKeyAPI   = "/api/v1/securitymanager/api"
	securityManagerOneTimeKeyAPI = "/api/v1/securitymanager/api/once"

	// api key renewal anticipation time (seconds)
	// please ensure that this variable is strictly superior to key validity
	// duration, else the auto-renewal won't work
	apiKeyRenewalAnticipation = 5
)

// insecure http client
var InsecureHTTPClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	},
	Timeout: time.Second * 10,
}

// check one time key, with manual informations gathering
func CheckOneTimeKeyManual(serviceIP string,
	oneTimeKey string,
	securityManagerAddress string) (*PublicKeyExport, error) {
	// build url
	url := "https://" +
		securityManagerAddress +
		":" +
		strconv.Itoa(securityManagerPort) +
		securityManagerBasicKeyAPI +
		"?publicKey=" +
		oneTimeKey

	// request key detail
	request, _ := http.NewRequest("GET",
		url,
		nil)

	// make request
	if response, err := InsecureHTTPClient.Do(request); err == nil {
		defer response.Body.Close()
		body, _ := ioutil.ReadAll(response.Body)
		if response.StatusCode == http.StatusOK {
			var key PublicKeyExport
			if err := json.Unmarshal(body,
				&key); err == nil {
				//if ( serviceIP != key.ServiceIP &&
				//		// if the service is running locally along with security manager...
				//		( key.ServiceIP != "127.0.0.1" &&
				//			serviceIP != "127.0.0.1" ) ) ||
				//	key.Type != KeyTypeOnce {
				//	return nil, errors.New( "client is impersonating" )
				//} else {
				return &key, nil
				//}
			} else {
				return nil, err
			}
		} else {
			return nil, errors.New("not 200 OK")
		}
	} else {
		return nil, err
	}
}

// check one time key from http request
func CheckOneTimeKey(request *http.Request,
	securityManagerAddress string) (*PublicKeyExport, error) {
	// get ip
	ip, _, _ := net.SplitHostPort(request.RemoteAddr)

	// extract one time key
	if oneTimeKey := request.Header.Get(OneTimeKeyShareHeaderName); oneTimeKey == "" {
		return nil, errors.New("one time key (X-Ghost-Token) is missing")
	} else {
		return CheckOneTimeKeyManual(ip,
			oneTimeKey,
			securityManagerAddress)
	}
}
