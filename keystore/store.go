package keystore

import (
	"math/rand"
)

// authorized characters into API keys
var AuthorizedAPIKeyCharacter = []rune("abcdefghijklmnopqrstuvwxyz-_1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ")

// generate a random API key of given length
func GenerateRandomKey(length int) string {
	out := make([]rune,
		length)
	for i := 0; i < length; i++ {
		out[i] = AuthorizedAPIKeyCharacter[rand.Int()%length]
	}
	return string(out)
}
