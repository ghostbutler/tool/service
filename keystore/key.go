package keystore

import (
	"encoding/json"
	"time"
)

const (
	OneTimeKeyShareHeaderName = "X-Ghost-Token"

	DefaultKeyDuration        = time.Minute * 5
	DefaultOneTimeKeyDuration = time.Second * 10
)

var KeyDuration = map[Type]time.Duration{
	KeyTypeAPI:  DefaultKeyDuration,
	KeyTypeOnce: DefaultOneTimeKeyDuration,
}

// key structure
type Key struct {
	// level
	Level string

	// service which registered
	ServiceIP   string
	ServiceName string

	// key
	PublicKey  string
	PrivateKey string

	// type
	Type Type
}

// a public json export of the key
type PublicKeyExport struct {
	// level
	Level string `json:"level"`

	// service which registered
	ServiceIP   string `json:"serviceIP"`
	ServiceName string `json:"serviceName"`

	// key
	PublicKey string `json:"publicKey"`

	// type
	Type Type `json:"type"`
}

// a private json export of the key
type PrivateKeyExport struct {
	// level
	Level string `json:"level"`

	// service which registered
	ServiceIP   string `json:"serviceIP"`
	ServiceName string `json:"serviceName"`

	// key
	PublicKey  string `json:"publicKey"`
	PrivateKey string `json:"privateKey"`

	// type
	Type Type `json:"type"`
}

// build key
func BuildKey(serviceName string,
	serviceIP string,
	_type Type,
	publicKey string,
	privateKey string,
	level string) *Key {
	return &Key{
		ServiceName: serviceName,
		ServiceIP:   serviceIP,
		Type:        _type,
		PublicKey:   publicKey,
		PrivateKey:  privateKey,
		Level:       level,
	}
}

// export complete key with private data to Json
func (key *Key) ExportPrivate() string {
	// build private key
	private := &PrivateKeyExport{
		Level:       key.Level,
		PublicKey:   key.PublicKey,
		PrivateKey:  key.PrivateKey,
		ServiceIP:   key.ServiceIP,
		ServiceName: key.ServiceName,
		Type:        key.Type,
	}

	// export to json
	export, _ := json.Marshal(private)
	return string(export)
}

// export key with only public data to Json
func (key *Key) ExportPublic() string {
	// build public key
	public := &PublicKeyExport{
		Level:       key.Level,
		PublicKey:   key.PublicKey,
		ServiceIP:   key.ServiceIP,
		ServiceName: key.ServiceName,
		Type:        key.Type,
	}

	// export to json
	export, _ := json.Marshal(public)
	return string(export)
}
