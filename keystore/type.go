package keystore

type Type int

const (
	KeyTypeAPI = Type(iota)
	KeyTypeOnce
)

var KeyTypeName = map[Type]string{
	KeyTypeAPI:  "normal",
	KeyTypeOnce: "once",
}

var KeyNameType = map[string]Type{
	"normal": KeyTypeAPI,
	"once":   KeyTypeOnce,
}
