Key store
=========

Introduction
------------

This library is for API key management, and API key structure which is common
to all projects.

Rule manager
------------

This tool allows a simple automatic add of new detection rule to scanner.

Key store
---------

Validity duration:

- Normal API key: 15 minutes
- One Time API Key: 10 seconds

Key validation
--------------

This library integrates a function called `CheckOneTimeKey`, which decides, based on request content, if this is a
valid one or not.

To validate request, it will:

- Extract the value of `X-Ghost-Butler-Key` http header (the one-time key)
- Send the extracted value to security manager which will return details about this one-time if it exists
- Read the received details about the key, and check if the IP for which key was delivered is the same then the one
doing the request

If each of this step are passed, the request is validated.

You can find built-in functions to achieves this into `check.go`:

- `CheckOneTimeKey( )`:
  
  You give to this function the http request, and the security manager address, and it will auto-check one time key
  validity. It will extract the `X-Ghost-Butler-Key` header, and ask to security manager for its validity.

Other functions
---------------

- `Login( )`:

  By passing the username/password you have for security manager login and its address, you get a normal API key if your
  username/password association is correct.
  
- `RequestOneTimeKey( )`:

  Given you current valid normal API key, and the security manager address, you get a one use API key. 

These two functions are called by `KeyManager` to perform auto key renewal.

Author
------

Ghost Butler <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/tool/service/keystore.git
