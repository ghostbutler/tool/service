package keystore

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/ghostbutler/tool/crypto"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

// login to security manager
func Login(username string,
	password string,
	securityManagerAddress string,
	serviceName string) (*Key, error) {
	// build url
	url := "https://" +
		securityManagerAddress +
		":" +
		strconv.Itoa(securityManagerPort) +
		securityManagerBasicKeyAPI

	// calculate sha512 hash for this password
	passwordHash := crypto.CalculateSHA512Hash([]byte(password))

	// encode login
	loginPhraseBase64 := base64.StdEncoding.EncodeToString([]byte(username +
		":" +
		passwordHash))

	// build request
	request, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte("serviceName="+
			serviceName)))
	request.Header.Add("Authorization",
		"Basic "+
			loginPhraseBase64)
	request.Header.Add("Content-Type",
		"application/x-www-form-urlencoded")

	// do request
	if response, err := InsecureHTTPClient.Do(request); err == nil {
		defer response.Body.Close()
		body, _ := ioutil.ReadAll(response.Body)
		if response.StatusCode == http.StatusOK {
			var exportKey PrivateKeyExport
			if err := json.Unmarshal(body,
				&exportKey); err == nil {
				key := &Key{
					ServiceIP:   exportKey.ServiceIP,
					Type:        exportKey.Type,
					PrivateKey:  exportKey.PrivateKey,
					PublicKey:   exportKey.PublicKey,
					Level:       exportKey.Level,
					ServiceName: exportKey.ServiceName,
				}
				return key, nil
			} else {
				return nil, err
			}
		} else {
			return nil, errors.New("bad credentials (got " +
				strconv.Itoa(response.StatusCode) +
				" response code)")
		}
	} else {
		return nil, err
	}
}

// request one time key
func RequestOneTimeKey(key *Key,
	securityManagerAddress string) (*PublicKeyExport, error) {
	// url
	url := "https://" +
		securityManagerAddress +
		":" +
		strconv.Itoa(securityManagerPort) +
		securityManagerOneTimeKeyAPI

	// build request
	request, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte("publicKey="+
			key.PublicKey+
			"&privateKey="+
			key.PrivateKey)))
	request.Header.Add("Content-Type",
		"application/x-www-form-urlencoded")

	// do request
	if response, err := InsecureHTTPClient.Do(request); err == nil {
		defer response.Body.Close()
		body, _ := ioutil.ReadAll(response.Body)
		if response.StatusCode == http.StatusOK {
			var key PublicKeyExport
			if err := json.Unmarshal(body,
				&key); err == nil {
				return &key, nil
			} else {
				return nil, err
			}
		} else {
			return nil, errors.New("bad credentials")
		}
	} else {
		return nil, err
	}
}

// key manager is the way to auto-manage your API key in time,
// it will renew it when it expires
type KeyManager struct {
	// security manager address
	securityManagerAddress string

	// login details
	username string
	password string

	// service name
	serviceName string

	// is updater running?
	isRunning bool

	// last delivery time
	lastDeliveryTime time.Time

	// current API key
	CurrentAPIKey *Key
}

// build key manager
func BuildKeyManager(securityManagerAddress string,
	username string,
	password string,
	serviceName string) *KeyManager {
	// output
	output := &KeyManager{
		securityManagerAddress: securityManagerAddress,
		isRunning:              true,
		CurrentAPIKey:          nil,
		username:               username,
		password:               password,
		serviceName:            serviceName,
	}

	// start update thread
	go output.updateThread()

	// ok
	return output
}

// close key manager
func (manager *KeyManager) Close() {
	manager.isRunning = false
}

// updater thread
func (manager *KeyManager) updateThread() {
	for manager.isRunning {
		// check key
		if manager.CurrentAPIKey == nil ||
			time.Now().Sub(manager.lastDeliveryTime) >= DefaultKeyDuration-(apiKeyRenewalAnticipation*time.Second) {
			// renew api key
			manager.renewApiKey()
		}

		// sleep
		time.Sleep(time.Second)
	}
}

// renew API key
func (manager *KeyManager) renewApiKey() {
	// get new api key
	if newApiKey, err := Login(manager.username,
		manager.password,
		manager.securityManagerAddress,
		manager.serviceName); err == nil {
		manager.CurrentAPIKey = newApiKey
		manager.lastDeliveryTime = time.Now()
	} else {
		fmt.Println("couldn't get api key:",
			err)
	}
}

// get one use key
func (manager *KeyManager) GetOneUseKey() (*PublicKeyExport, error) {
	if manager.CurrentAPIKey == nil {
		return nil, errors.New("no api key")
	} else {
		if key, err := RequestOneTimeKey(manager.CurrentAPIKey,
			manager.securityManagerAddress); err == nil {
			return key, err
		} else {
			manager.CurrentAPIKey = nil
			return nil, err
		}
	}
}
