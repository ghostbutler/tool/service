package rabbit

import (
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"time"
)

type Controller struct {
	// connection
	Connection *amqp.Connection

	// channel
	Channel *amqp.Channel

	// host
	Host []string

	// credentials
	username, password string
}

// build RabbitMQ controller
func BuildController(host []string,
	username string,
	password string) (*Controller, error) {
	// allocate
	controller := &Controller{
		username: username,
		password: password,
		Host:     host,
	}

	// update one time
	if err := controller.update(); err != nil {
		return nil, err
	}
	// run update thread
	go controller.updateThread()

	// done
	return controller, nil
}

// close
func (controller *Controller) Close() {
	_ = controller.Connection.Close()
}

// connect
func (controller *Controller) connect() error {
	for _, host := range controller.Host {
		if conn, err := amqp.Dial("amqp://" +
			controller.username +
			":" +
			controller.password +
			"@" +
			host); err == nil {
			controller.Connection = conn
			if channel, err := conn.Channel(); err == nil {
				controller.Channel = channel
				return nil
			} else {
				fmt.Println("couldn't create channel to " +
					host +
					" : " +
					err.Error())
				continue
			}
		} else {
			fmt.Println("couldn't connect to " +
				host +
				" : " +
				err.Error())
			continue
		}
	}
	return errors.New("couldn't connect to any host")
}

// update
func (controller *Controller) update() error {
	if controller.Connection != nil {
		if controller.Connection.IsClosed() {
			controller.Connection = nil
		}
	}
	if controller.Connection == nil {
		if err := controller.connect(); err != nil {
			return err
		}
	}
	return nil
}

func (controller *Controller) updateThread() {
	for {
		if err := controller.update(); err != nil {
			fmt.Println("rabbitmq:" +
				err.Error())
		}
		time.Sleep(time.Millisecond * 1000)
	}
}
