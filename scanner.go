package common

// scanner results list
type ScannerResult struct {
	IP       string `json:"ip"`
	RuleName string `json:"rule"`
}

// scanner result data
type ScannerResultList struct {
	Result []ScannerResult `json:"result"`
}

// build rule name
func (result *ScannerResult) GetRuleName() string {
	return result.RuleName +
		":" +
		result.IP
}
