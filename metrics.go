package common

import (
	"github.com/prometheus/client_golang/prometheus"
)

type Metrics struct {
	TimePerRequestProcessing *prometheus.SummaryVec
	HTTPRequestSize          *prometheus.SummaryVec
	TotalHTTPRequest         *prometheus.CounterVec
}

// build metrics
func buildMetrics() *Metrics {
	// allocate
	metrics := &Metrics{}

	// total http requests counter
	metrics.TotalHTTPRequest = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total processed http requests",
			ConstLabels: prometheus.Labels{
				"type": "ghost",
			},
		},
		[]string{
			"path",
			"method",
			"code",
		},
	)

	// request size
	metrics.HTTPRequestSize = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "http_request_size_bytes",
			Help: "incoming requests size (bytes)",
			ConstLabels: prometheus.Labels{
				"type": "ghost",
			},
		},
		[]string{
			"path",
			"method",
			"code",
		},
	)

	// delay to process request
	metrics.TimePerRequestProcessing = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "http_request_duration_microseconds",
			Help: "processing time of incoming requests in microseconds",
			ConstLabels: prometheus.Labels{
				"type": "ghost",
			},
		},
		[]string{
			"path",
			"method",
			"code",
		},
	)

	// register
	prometheus.MustRegister(metrics.TotalHTTPRequest)
	prometheus.MustRegister(metrics.TimePerRequestProcessing)
	prometheus.MustRegister(metrics.HTTPRequestSize)

	// done
	return metrics
}
