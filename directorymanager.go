package common

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"net/http"
	"strconv"
	"time"
)

type DirectoryManager struct {
	// key store
	keyManager *keystore.KeyManager

	// directory hostname
	directoryHostname string

	// last send time
	lastContactTime time.Time

	// is running?
	isRunning bool
}

const (
	DelayBetweenDirectoryContact = time.Second * 2
)

// directory manager
func BuildDirectoryManager(directoryHostname string,
	keyManager *keystore.KeyManager) *DirectoryManager {
	// allocate
	directoryManager := &DirectoryManager{
		lastContactTime:   time.Now(),
		isRunning:         true,
		directoryHostname: directoryHostname,
		keyManager:        keyManager,
	}

	// start update thread
	go directoryManager.updateThread()

	// done
	return directoryManager
}

// update
func (directoryManager *DirectoryManager) updateThread() {
	for directoryManager.isRunning {
		if time.Now().Sub(directoryManager.lastContactTime) >= DelayBetweenDirectoryContact {
			// build request
			request, _ := http.NewRequest("POST",
				"https://"+
					directoryManager.directoryHostname+
					":"+
					strconv.Itoa(GhostService[ServiceDirectory].DefaultPort)+
					"/api/v1/directory/notify",
				nil)

			// notify itself
			if key, err := directoryManager.keyManager.GetOneUseKey(); err == nil {
				// add one time key
				request.Header.Add(keystore.OneTimeKeyShareHeaderName,
					key.PublicKey)

				// send request
				DiscardHTTPResponse(InsecureHTTPClient.Do(request))
			} else {
				fmt.Println("couldn't get one time key:",
					err)
			}

			// save update
			directoryManager.lastContactTime = time.Now()
		}

		// sleep
		time.Sleep(time.Millisecond * 100)
	}
}

type DirectoryAction struct {
	// the name of the actuator of the given type to
	// act on
	// no name means all actuators of given type
	Name string `json:"name""`

	// action identifier
	Identifier orchestration.ActionIdentifier `json:"identifier"`

	// field
	FieldList map[string]interface{} `json:"field"`
	
	// device type
	Type string `json:"type"`
}

func ( directoryAction *DirectoryAction ) MarshalJson( ) [ ]byte {
	output, _ := json.Marshal( directoryAction )
	return output
}
