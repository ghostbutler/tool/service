package device

import (
	"encoding/json"
)

type MusicEventType string

const (
	MusicFileEventTypeLoad   = MusicEventType("load")
	MusicFileEventTypeRemove = MusicEventType("remove")
	MusicFileEventTypePlay   = MusicEventType("play")
	MusicFileEventTypePause  = MusicEventType("pause")
	MusicFileEventTypeVolume = MusicEventType("volume")
	MusicFileEventTypeStop   = MusicEventType("stop")
	MusicFileEventTypeFinish = MusicEventType("finish")
	MusicFileEventTypeTest = MusicEventType("test")
)

type MusicEvent struct {
	// player name
	Name string `json:"name" bson:"name"`

	// event type (remove/add/play/volume/pause/stop/finish)
	Event MusicEventType `json:"event" bson:"event"`

	// music name
	MusicName string `json:"musicName" bson:"musicName"`

	// volume
	Volume int `json:"volume" bson:"volume"`

	// is warning?
	IsWarning bool `json:"isWarning" bson:"isWarning"`

	// url
	URL string `json:"url" bson:"url"`
}

func (musicEvent MusicEvent) MarshalJSON() []byte {
	output, _ := json.Marshal(&musicEvent)
	return output
}
