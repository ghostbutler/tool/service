package device

import (
	"encoding/json"
	"errors"
	"time"
)

type ExportMode string

const (
	ExportModeAction = ExportMode("action")
	ExportModeData   = ExportMode("data")
)

type Export struct {
	// type
	Type string `json:"type"`

	// content
	Content string `json:"content"`

	// time
	Time string `json:"time"`
}

// marshal
func DoExport(_type CapabilityType,
	content []byte) []byte {
	data, _ := json.Marshal(Export{
		Type:    CapabilityTypeName[_type],
		Content: string(content),
		Time:    time.Now().Format(time.RFC3339Nano),
	})
	return data
}

type SensorData struct {
	Type string      `bson:"type"`
	Data interface{} `bson:"data"`
	Time time.Time   `bson:"time"`
	Name string      `json:"-" bson:"-"`
}

func (sensorData *SensorData) ParseData() error {
	if data, err := json.Marshal(sensorData.Data); err == nil {
		switch sensorData.Type {
		case CapabilityTypeName[CapabilityTypeLight]:
			var final Light
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			break
		case CapabilityTypeName[CapabilityTypeOutlet]:
			var final Outlet
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			break
		case CapabilityTypeName[CapabilityTypeGamepad]:
			var final Switch
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			data, _ = json.Marshal(sensorData.Data.(*Switch).Event)
			switch sensorData.Data.(*Switch).Type {
			case SwitchEventTypeButton:
				var final Button
				if err := json.Unmarshal(data,
					&final); err != nil {
					return err
				}
				sensorData.Data.(*Switch).Event = &final
				break

			case SwitchEventTypeJoystick:
				var final Joystick
				if err := json.Unmarshal(data,
					&final); err != nil {
					return err
				}
				sensorData.Data.(*Switch).Event = &final
				break

			default:
				return errors.New("unknown gamepad event type")
			}
			break

		case CapabilityTypeName[CapabilityTypeTextToSpeech]:
			var final TTS
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			break

		case CapabilityTypeName[CapabilityTypeMusic]:
			var final MusicEvent
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			break

		case CapabilityTypeName[CapabilityTypeTemperature]:
			var final TemperatureEvent
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			break

		case CapabilityTypeName[CapabilityTypeMoisture]:
			var final MoistureEvent
			if err := json.Unmarshal(data,
				&final); err != nil {
				return err
			}
			sensorData.Data = &final
			sensorData.Name = final.Name
			break

		default:
			return errors.New("unknown event type")
		}
	} else {
		return err
	}
	return nil
}
