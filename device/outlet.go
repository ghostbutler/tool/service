package device

type Outlet struct {
	IP          string  `json:"-" bson:"-"`
	Name        string  `json:"name" bson:"name"`
	Port        int     `json:"port" bson:"port"`
	ID          string  `json:"-" bson:"-"`
	Output      int     `json:"output" bson:"output"`
	Power       float64 `json:"power" bson:"power"`
	Energy      float64 `json:"energy" bson:"energy"`
	Enabled     int     `json:"enabled" bson:"enabled"`
	Current     float64 `json:"current" bson:"current"`
	Voltage     float64 `json:"voltage" bson:"voltage"`
	PowerFactor float64 `json:"powerFactor" bson:"powerFactor"`
}
