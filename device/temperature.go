package device

type TemperatureEvent struct {
	Name string `json:"name" bson:"name"`
	Temperature int `json:"temperature" bson:"temperature"`
}
