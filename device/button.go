package device

type ButtonStatus string

const (
	ButtonStatusPressed       = ButtonStatus("pressed")
	ButtonStatusLongPressed   = ButtonStatus("lpressed")
	ButtonStatusReleased      = ButtonStatus("released")
	ButtonStatusLongReleased  = ButtonStatus("lreleased")
	ButtonStatusDoublePressed = ButtonStatus("dpressed")
)

type Button struct {
	Index  int          `json:"-" bson:"-"`
	Name   string       `json:"name" bson:"name"`
	Status ButtonStatus `json:"status" bson:"status"`
}
