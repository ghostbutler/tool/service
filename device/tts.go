package device

import(
	"encoding/json"

)
type TTS struct {
	Name string `json:"name" bson:"name"`
	Sentence string `json:"sentence" bson:"sentence"`
	Language string `json:"language" bson:"language"`
	Volume int `json:"volume" bson:"volume"`
}

// marshal
func (tts *TTS) MarshalJson() []byte {
	result, _ := json.Marshal(tts)
	return DoExport(CapabilityTypeTextToSpeech,
		result)
}
