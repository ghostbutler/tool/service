package device

import (
	"encoding/json"
)

type SwitchEventType string

const (
	SwitchEventTypeButton   = SwitchEventType("button")
	SwitchEventTypeJoystick = SwitchEventType("joystick")
)

type Switch struct {
	Id          string          `json:"-" bson:"-"`
	Name        string          `json:"name" bson:"name"`
	LastUpdate  string          `json:"lastUpdate" bson:"lastUpdate"`
	Event       interface{}     `json:"event" bson:"event"` // can be either Button or Joystick
	Type        SwitchEventType `json:"type" bson:"type"`
	IsReachable bool            `json:"isReachable" bson:"isReachable"`
	UUID        string          `json:"uuid" bson:"uuid"`

	IsMustSave bool `json:"-" bson:"-"`
}

func (sw *Switch) MarshalJson() []byte {
	content, _ := json.Marshal(sw)
	return DoExport(CapabilityTypeGamepad,
		content)
}
