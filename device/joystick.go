package device

type Joystick struct {
	Index int     `json:"-" bson:"-"`
	Name  string  `json:"name" bson:"name"`
	X     float64 `json:"X" bson:"X"`
	Y     float64 `json:"Y" bson:"Y"`
}
