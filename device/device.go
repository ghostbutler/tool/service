package device

type CapabilityType int

const (
	CapabilityTypeLight = CapabilityType(iota)
	CapabilityTypeOutlet
	CapabilityTypeGamepad
	CapabilityTypeMusic
	CapabilityTypeTextToSpeech
	CapabilityTypeTemperature
	CapabilityTypeMoisture

	CapabilityTypes
)

// device type name
var CapabilityTypeName = map[CapabilityType]string{
	CapabilityTypeLight:        "light",
	CapabilityTypeOutlet:       "outlet",
	CapabilityTypeGamepad:      "gamepad",
	CapabilityTypeMusic:        "music",
	CapabilityTypeTextToSpeech: "texttospeech",
	CapabilityTypeTemperature:  "temperature",
	CapabilityTypeMoisture:     "moisture",
}

// capability devices names url
var CapabilityTypeListURL = map[CapabilityType]string{
	CapabilityTypeLight:        "/api/v1/controller/lights/list",
	CapabilityTypeOutlet:       "/api/v1/controller/outlets/list",
	CapabilityTypeGamepad:      "/api/v1/controller/gamepads/list",
	CapabilityTypeMusic:        "/api/v1/controller/name",
	CapabilityTypeTextToSpeech: "/api/v1/controller/name",
	CapabilityTypeTemperature:  "/api/v1/controller/name",
	CapabilityTypeMoisture:     "/api/v1/controller/name",
}

// find capability from name
// returns CapabilityTypes if not found
func FindCapabilityFromName(capabilityNameToFind string) CapabilityType {
	// iterate
	for capabilityType, capabilityName := range CapabilityTypeName {
		if capabilityName == capabilityNameToFind {
			return capabilityType
		}
	}

	// not found
	return CapabilityTypes
}

// test if a given capability does exist
func IsCapacityValid(capabilityNameToFind string) bool {
	if res := FindCapabilityFromName(capabilityNameToFind); res == CapabilityTypes {
		return false
	} else {
		return true
	}
}

type DeviceControllerAddress struct {
	Hostname string `json:"hostname"`
	Port     int    `json:"port"`
}

type DeviceController struct {
	Type       string                  `json:"type"`
	Controller DeviceControllerAddress `json:"controller"`
	Name       string                  `json:"name"`
}

type ActiveSensor struct {
	CapabilityType string `json:"type"`
	Name string `json:"name"`
}
