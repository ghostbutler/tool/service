package device

import (
	"encoding/json"
)

type LightType string

const (
	LightTypeBulb   = LightType("bulb")
	LightTypeGlobe  = LightType("globe")
	LightTypeStrip  = LightType("strip")
	LightTypeCandle = LightType("candle")
)

var LightTypeName = map[string]LightType{
	"LCT015": LightTypeBulb,
	"LLC020": LightTypeGlobe,
	"LST002": LightTypeStrip,
	"LCT012": LightTypeCandle,
}

type LightState struct {
	// hue
	HUE int `json:"hue" bson:"hue"`

	// brightness
	Brightness int `json:"brightness" bson:"brightness"`

	// saturation
	Saturation int `json:"saturation" bson:"saturation"`

	// on
	IsOn bool `json:"isOn" bson:"isOn"`

	// is reachable
	IsReachable bool `json:"isReachable" bson:"isReachable"`
}

type Light struct {
	// uuid
	UUID string `json:"uuid" bson:"uuid"`

	// name
	Name string `json:"name" bson:"name"`

	// type
	Type LightType `json:"type" bson:"type"`

	// status
	State LightState `json:"state" bson:"state"`

	// id
	Id string `json:"-" bson:"-"`

	// is must save (status != then previous one)
	IsMustSave bool `json:"-" bson:"-"`
}

// marshal
func (light *Light) MarshalJson() []byte {
	result, _ := json.Marshal(light)
	return DoExport(CapabilityTypeLight,
		result)
}
