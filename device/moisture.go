package device

import(
	"encoding/json"
)

type MoistureEvent struct {
	Name string `json:"name" bson:"name"`

	// moisture value between 0 and 1024
	Moisture int `json:"moisture" bson:"moisture"`
}

func (event *MoistureEvent) MarshalJson() [ ]byte {
	output, _ := json.Marshal( event )
	return output
}
