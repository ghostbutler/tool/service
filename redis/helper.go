package redis

import (
	"github.com/bsm/redis-lock"
)

func Unlock(locker *lock.Locker) {
	_ = locker.Unlock()
}
