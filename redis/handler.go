package redis

import (
	"github.com/bsm/redis-lock"
	"github.com/go-redis/redis"
	"time"
)

type RedisMode int

const (
	RedisModeNormal = RedisMode(iota)
	RedisModeCluster
)

const (
	DelayBeforeGetLockTimeout  = time.Second * 10
	RedisLockMaximumRetryCount = 10
)

type Handler struct {
	// mode
	redisMode RedisMode

	// client
	Client redis.Cmdable
}

// build redis handler
// one hostname is redis in normal mode, mode than one, cluster mode
func BuildHandler(redisHostname []string,
	redisPassword string) (*Handler, error) {
	// allocate
	databaseHandler := &Handler{}

	// build cluster client
	if len(redisHostname) <= 1 {
		databaseHandler.redisMode = RedisModeNormal
		databaseHandler.Client = redis.NewClient(&redis.Options{
			Password: redisPassword,
			Addr:     redisHostname[0],
		},
		)
	} else {
		databaseHandler.redisMode = RedisModeCluster
		databaseHandler.Client = redis.NewClusterClient(&redis.ClusterOptions{
			Password: redisPassword,
			Addrs:    redisHostname,
		},
		)
	}

	// test database connectivity
	if _, err := databaseHandler.Client.Ping().Result(); err != nil {
		return nil, err
	}

	// done
	return databaseHandler, nil
}

// lock
func (handler *Handler) ObtainLock(key string) (*lock.Locker, error) {
	return lock.Obtain(handler.Client,
		key,
		&lock.Options{
			LockTimeout: DelayBeforeGetLockTimeout,
			RetryCount:  RedisLockMaximumRetryCount,
		})
}
