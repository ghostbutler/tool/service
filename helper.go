package common

import (
	"crypto/tls"
	"io"
	"net/http"
	"strconv"
	"time"
)

var BoolMap = map[bool]string{
	false: "false",
	true:  "true",
}

// insecure http client
var InsecureHTTPClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	},
	Timeout: time.Second * 10,
}

// discard http response
func DiscardHTTPResponse(response *http.Response,
	err error) {
	// check for error
	if err == nil {
		Close(response.Body)
	}
}

// close io reader
func Close(reader io.ReadCloser) {
	_ = reader.Close()
}

// extract form value
func ExtractFormValue(request *http.Request,
	parameterName string) string {
	if valueList, ok := request.Form[parameterName]; ok {
		return valueList[0]
	} else {
		return ""
	}
}

func IsInt( s string ) bool {
	if _, err := strconv.Atoi( s ); err == nil {
		return true
	} else {
		return false
	}
}
