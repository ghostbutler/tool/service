package rule

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const (
	controllerRuleSendDelay = time.Second * 10
	scannerPort             = 16561
)

type RuleManager struct {
	// rules set
	Rule []string

	// scanner address
	Address string

	// last send time
	LastSendTime time.Time

	// is update thread running
	isRunning bool

	// key manager
	keyManager *keystore.KeyManager
}

// build rule
func BuildRuleManager(scannerAddress string,
	keyManager *keystore.KeyManager,
	rule []string) *RuleManager {
	// build rule manager
	ruleManager := &RuleManager{
		Address:      scannerAddress,
		Rule:         rule,
		isRunning:    true,
		keyManager:   keyManager,
		LastSendTime: time.Now().Add(-1 * time.Second * 5),
	}

	// start update thread
	go ruleManager.updateThread()

	// ok
	return ruleManager
}

// send to scanner
func (manager *RuleManager) sendToScanner() {
	// build insecure http(s)
	httpClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	for _, rule := range manager.Rule {
		// build body
		body := bytes.NewBuffer([]byte(rule))

		// build request
		request, _ := http.NewRequest("POST",
			"https://"+
				manager.Address+
				":"+
				strconv.Itoa(scannerPort)+
				"/api/v1/scanner/register",
			body)

		// get api key
		if key, err := manager.keyManager.GetOneUseKey(); err == nil {
			// add key to request
			request.Header.Add(keystore.OneTimeKeyShareHeaderName,
				key.PublicKey)

			// send request
			if response, err := httpClient.Do(request); err == nil {
				_, _ = io.Copy(ioutil.Discard,
					response.Body)
				_ = response.Body.Close()
				if response.StatusCode != http.StatusOK {
					fmt.Println("couldn't add rule to scanner:", response.StatusCode)
				}
			} else {
				fmt.Println("couldn't send rule to scanner:",
					err)
			}
		} else {
			fmt.Println("couldn't get one time api key")
		}
	}
}

// update thread
func (manager *RuleManager) updateThread() {
	// update thread
	for manager.isRunning {
		// check last send time
		if time.Now().Sub(manager.LastSendTime) > controllerRuleSendDelay {
			// send to scanner
			manager.sendToScanner()

			// save time
			manager.LastSendTime = time.Now()
		}

		// sleep
		time.Sleep(time.Millisecond * 16)
	}
}
