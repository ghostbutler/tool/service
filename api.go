package common

import (
	"crypto/sha512"
	"crypto/tls"
	"encoding/json"
	"github.com/denisbrodbeck/machineid"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/ghostbutler/tool/crypto"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/status"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	BadRequestMessage = "{\"error\": \"unknown request\"}"
)

const (
	ServiceIDHTTPHeader = "X-Service-Id"
)

type APIServiceType int

const (
	APIServiceV1BuiltInEndpointList = APIServiceType(iota)
	APIServiceV1BuiltInStatus
	APIServiceV1BuiltInCapabilities
	APIServiceV1BuiltInMetrics

	APIServiceBuiltInLast
)

type EndpointFunction func(rw http.ResponseWriter,
	request *http.Request,
	service *Service,
	handle interface{}) (responseStatusCode int)

type APIEndpoint struct {
	// path to service
	Path []string

	// method to access service
	Method string

	// description
	Description string

	// callback
	Callback EndpointFunction

	// must provide one time key?
	IsMustProvideOneTimeKey bool
}

var APIService = map[APIServiceType]*APIEndpoint{
	APIServiceV1BuiltInEndpointList: &APIEndpoint{
		Path:                    []string{"api", "v1"},
		Method:                  "GET",
		Description:             "Give a complete list of all the endpoints existing",
		Callback:                endpointListBuildInEndpoint,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1BuiltInStatus: &APIEndpoint{
		Path:                    []string{"api", "v1", "status"},
		Method:                  "GET",
		Description:             "Get the current status of the service",
		Callback:                statusBuiltInEndpoint,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1BuiltInCapabilities: {
		Path:                    []string{"api", "v1", "capabilities"},
		Method:                  "GET",
		Description:             "Give a complete list of all the capabilities of this controller",
		Callback:                endpointListCapabilityBuiltInEndpoint,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1BuiltInMetrics: {
		Path:                    []string{"api", "v1", "metrics"},
		Method:                  "GET",
		Description:             "Output prometheus formatted metrics about service",
		Callback:                endpointPrometheusMetricsExport,
		IsMustProvideOneTimeKey: false,
	},
}

type Service struct {
	// endpoints
	APIService map[APIServiceType]*APIEndpoint

	// metrics
	Metrics *Metrics

	// certificate
	certificate *tls.Certificate

	// status service
	status *status.Status

	// default callback
	defaultEndpoint EndpointFunction

	// handle to pass to the callbacks
	handle interface{}

	// service capabilities
	capabilityList []device.CapabilityType

	// security manager hostname
	securityManagerHostname string
}

// default endpoint for failed request
func defaultBuiltInEndpoint(rw http.ResponseWriter,
	_ *http.Request,
	_ *Service,
	_ interface{}) int {
	rw.WriteHeader(http.StatusBadRequest)
	_, _ = rw.Write([]byte(BadRequestMessage))
	return http.StatusBadRequest
}

// list endpoint
func endpointListBuildInEndpoint(rw http.ResponseWriter,
	_ *http.Request,
	service *Service,
	_ interface{}) int {
	// endpoint description
	type EndpointDescription struct {
		Method      string `json:"method"`
		Path        string `json:"endpoint"`
		Description string `json:"description"`
	}

	// endpoints list
	var endpointList struct {
		// endpoints
		Endpoint []EndpointDescription `json:"endpoints"`
	}
	endpointList.Endpoint = make([]EndpointDescription,
		0,
		1)

	// fill with endpoints
	for _, service := range service.APIService {
		endpointList.Endpoint = append(endpointList.Endpoint,
			EndpointDescription{
				Method: service.Method,
				Path: "/" +
					strings.Join(service.Path,
						"/"),
				Description: service.Description,
			},
		)
	}

	// marshal
	data, _ := json.MarshalIndent(&endpointList,
		"",
		"\t")

	// output to stream
	_, _ = rw.Write(data)

	// done
	return http.StatusOK
}

// status request
func statusBuiltInEndpoint(rw http.ResponseWriter,
	_ *http.Request,
	service *Service,
	_ interface{}) int {
	rw.Header().Add(ServiceIDHTTPHeader,
		service.status.ID)
	_, _ = rw.Write(service.status.GenerateJson())
	return http.StatusOK
}

// capabilities request
func endpointListCapabilityBuiltInEndpoint(rw http.ResponseWriter,
	_ *http.Request,
	service *Service,
	_ interface{}) int {
	// capabilities
	capabilityList := make([]string,
		0,
		1)

	// fill with capabilities
	for _, capability := range service.capabilityList {
		capabilityList = append(capabilityList,
			device.CapabilityTypeName[capability])
	}

	// marshal data
	data, _ := json.Marshal(capabilityList)

	// send data
	_, _ = rw.Write(data)

	// done
	return http.StatusOK
}

func endpointPrometheusMetricsExport(rw http.ResponseWriter,
	request *http.Request,
	_ *Service,
	_ interface{}) int {
	promhttp.Handler().ServeHTTP(rw,
		request)
	return http.StatusOK
}

// build service
// listeningPort is the port service will listen on
// service type is one of the services defined as common.ServiceType
// endpoint are the endpoints available for the service
// versionMajor/versionMinor are the current service version
// defaultEndpoint is the callback to which delegate request if common-built-in endpoints doesn't know how to
// process request. if this is set to nil, default endpoint will be used
// handle is the object you wish to pass to the callbacks
func BuildService(listeningPort int,
	serviceType ServiceType,
	endpoint map[APIServiceType]*APIEndpoint,
	versionMajor int,
	versionMinor int,
	defaultEndpoint EndpointFunction,
	capabilityList []device.CapabilityType,
	securityManagerHostName string,
	handle interface{}) *Service {
	// allocate
	service := &Service{
		APIService: make(map[APIServiceType]*APIEndpoint),
		status: status.Build(versionMajor,
			versionMinor,
			GhostServiceTypeName,
			GhostService[serviceType].Name),
		defaultEndpoint:         defaultEndpoint,
		handle:                  handle,
		capabilityList:          capabilityList,
		securityManagerHostname: securityManagerHostName,
		Metrics:                 buildMetrics(),
	}

	// check capabilities list
	if service.capabilityList == nil {
		service.capabilityList = make([]device.CapabilityType,
			0,
			1)
	}

	// extract machine-only data
	id, _ := machineid.ID()
	hashHandler := sha512.New()
	hashHandler.Write([]byte(id))
	total := int64(0)
	hash := hashHandler.Sum(nil)
	for _, b := range hash {
		total += int64(b)
	}

	// set random seed
	rand.Seed(time.Now().UnixNano() + total)

	// check endpoint
	if service.defaultEndpoint == nil {
		service.defaultEndpoint = defaultBuiltInEndpoint
	}

	// concatenate endpoints with default endpoints
	if endpoint != nil {
		for srv, endpoint := range endpoint {
			service.APIService[srv] = endpoint
		}
	}
	for srv, endpoint := range APIService {
		service.APIService[srv] = endpoint
	}

	// generate certificate
	service.certificate, _ = crypto.GenerateDefaultX509KeyPair()

	// listen and serve
	go crypto.ListenAndServeTLSKeyPair(":"+
		strconv.Itoa(listeningPort),
		service.certificate,
		service)

	// done
	return service
}

// cut request path
func cutRequestPath(requestPath string) []string {
	path := strings.Split(requestPath,
		"/")
	for i := 0; i < len(path); {
		if path[i] == "" {
			if len(path) > 1 {
				path = append(path[:i],
					path[i+1:]...)
			} else {
				return []string{}
			}
		} else {
			i++
		}
	}
	return path
}

// are path equals?
func arePathEqual(path []string,
	ref []string) bool {
	if len(path) == len(ref) {
		for i := 0; i < len(ref); i++ {
			if path[i] != ref[i] {
				return false
			}
		}
		return true
	}
	return false
}

// find endpoint
func (service *Service) findEndpoint(request *http.Request) *APIEndpoint {
	// cut path
	path := cutRequestPath(request.URL.Path)

	// check path
	//if len(path) <= 0 {
	//	return nil
	//}

	// iterate endpoints
	for _, endpoint := range service.APIService {
		if arePathEqual(endpoint.Path,
			path) &&
			endpoint.Method == request.Method {
			return endpoint
		}
	}

	// no endpoint found
	return nil
}

// compute request size (comes from prometheus http example)
func computeApproximateRequestSize(r *http.Request) <-chan int {
	// Get URL length in current goroutine for avoiding a race condition.
	// HandlerFunc that runs in parallel may modify the URL.
	s := 0
	if r.URL != nil {
		s += len(r.URL.String())
	}

	out := make(chan int, 1)

	go func() {
		s += len(r.Method)
		s += len(r.Proto)
		for name, values := range r.Header {
			s += len(name)
			for _, value := range values {
				s += len(value)
			}
		}
		s += len(r.Host)
		if r.ContentLength != -1 {
			s += int(r.ContentLength)
		}
		out <- s
		close(out)
	}()

	return out
}

// serve http request
func (service *Service) ServeHTTP(rw http.ResponseWriter,
	request *http.Request) {
	initialTime := time.Now()
	defer request.Body.Close()
	var responseCode int
	requestSizeChannel := computeApproximateRequestSize(request)
	if endpoint := service.findEndpoint(request); endpoint != nil {
		if endpoint.IsMustProvideOneTimeKey {
			if _, err := keystore.CheckOneTimeKey(request,
				service.securityManagerHostname); err == nil {
				responseCode = endpoint.Callback(rw,
					request,
					service,
					service.handle)
			} else {
				rw.WriteHeader(http.StatusUnauthorized)
				responseCode = http.StatusUnauthorized
			}
		} else {
			responseCode = endpoint.Callback(rw,
				request,
				service,
				service.handle)
		}
	} else {
		responseCode = service.defaultEndpoint(rw,
			request,
			service,
			service.handle)
	}
	_, _ = io.Copy(ioutil.Discard,
		request.Body)

	// monitor
	service.Metrics.HTTPRequestSize.WithLabelValues(request.URL.Path,
		request.Method,
		strconv.Itoa(responseCode)).Observe(float64(<-requestSizeChannel))
	service.Metrics.TotalHTTPRequest.WithLabelValues(request.URL.Path,
		request.Method,
		strconv.Itoa(responseCode)).Add(1)
	duration := time.Now().Sub(initialTime)
	service.Metrics.TimePerRequestProcessing.WithLabelValues(request.URL.Path,
		request.Method,
		strconv.Itoa(responseCode)).Observe(float64(duration / time.Microsecond))
}
